package AntDemo;

public class Ant {
	private String name;
	
	public Ant(String n) {
		this.name = n;
	}
	
	public void move() {
		System.out.println(this.name + " Ant is moving");
	}

}
