package AntDemo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class AntTest {
	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private final PrintStream standardOut = System.out;
	
	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(baos));
	}

	@AfterEach
	public void tearDown() {
		System.setOut(standardOut);
	}
	
	
	@Test
	void testMove() {
		Ant an = new Ant("mike");
		an.move();
		assertEquals("mike Ant is moving", baos.toString().trim());
	}
}

